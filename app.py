"""
demo.py
Antes de ejecutar el programa establece unas variables de entorno:
    PYTHON_USERNAME       - El ususario de la base de datos
    PYTHON_PASSWORD       - La contraseña del usuario
    PYTHON_CONNECTSTRING  - El nombre o dirección del host donde se encuentra y el nombre de la sesión. "example.com/XEPDB1"
    PORT                  - El puerto por el que se va a servir la app web, por defecto es el 8080 
"""
# Importamos las funciones del sistema, el módulo que conecta python con oracle y flask para la aplicación en cuestión.
import os
import sys
import cx_Oracle
from flask import Flask, render_template, abort,request
from flask import Flask, request
from http import HTTPStatus
import copy
from flask_cors import CORS
from flask_mail import Mail, Message

################################################################################
# Esta parte del código es por si se inicia en otro sistema operativo.

if sys.platform.startswith("darwin"):
    cx_Oracle.init_oracle_client(lib_dir=os.environ.get("HOME")+"/instantclient_19_3")
elif sys.platform.startswith("win32"):
    cx_Oracle.init_oracle_client(lib_dir=r"c:\oracle\instantclient_19_8")


# Init_session es una función para una llamada de sesión eficiente.

def init_session(connection, requestedTag_ignored):
    cursor = connection.cursor()
    cursor.execute("""
        ALTER SESSION SET
          TIME_ZONE = 'UTC'
          NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI'""")

# Función para establecer la conexión con la base de datos
def start_pool():

    # Creamos las conexión con la base de datos usando variables de entorno.
    # Las variables de entorno en este caso son 
        #[oracle@bd ~]$ export PYTHON_CONNECTSTRING=localhost/xe
        #[oracle@bd ~]$ export PYTHON_PASSWORD=empleado
        #[oracle@bd ~]$ export PYTHON_USERNAME=empleado
    
    pool_min = 4
    pool_max = 4
    pool_inc = 0
    pool_gmd = cx_Oracle.SPOOL_ATTRVAL_WAIT

    print("Connecting to", os.environ.get("PYTHON_CONNECTSTRING"))

    pool = cx_Oracle.SessionPool(user=os.environ.get("PYTHON_USERNAME"),
                                 password=os.environ.get("PYTHON_PASSWORD"),
                                 dsn=os.environ.get("PYTHON_CONNECTSTRING"),
                                 min=pool_min,
                                 max=pool_max,
                                 increment=pool_inc,
                                 threaded=True,
                                 getmode=pool_gmd,
                                 sessionCallback=init_session)

    return pool

def insertar(conexion, nombre, apellido, password, correo, cedula):
    cursor=conexion.cursor() 
    datos="insert into empleadosf values( null,'{}','{}','{}','{}','{}')".format(nombre,apellido,password,correo,cedula)
    print(datos)
    cursor.execute(datos)
    cursor.execute("select * from empleadosf where codempleado = (select max(CODEMPLEADO) from empleadosf)")
    cursor.rowfactory = lambda *args: dict(zip([d[0] for d in cursor.description], args))
    row = cursor.fetchone()
    conexion.commit()
    cursor.close()
    conexion.close()
    return row

def actualizar(conexion, codempleado, nombre, apellido, password, correo, cedula):
    cursor=conexion.cursor() 
    datos="update empleadosf set nombre='{}',apellido='{}',password='{}',correo='{}',cedula='{}' WHERE codempleado='{}'".format(nombre,apellido,password,correo,cedula,codempleado)
    print(datos)
    cursor.execute(datos)
    cursor.execute("SELECT * FROM empleadosf where codempleado="+str(codempleado))
    cursor.rowfactory = lambda *args: dict(zip([d[0] for d in cursor.description], args))
    row = cursor.fetchone()
    conexion.commit()
    cursor.close()
    conexion.close()
    return row

def eliminar(conexion, codempleado):
    cursor=conexion.cursor() 
    cursor.execute("SELECT * FROM empleadosf where codempleado="+str(codempleado))
    cursor.rowfactory = lambda *args: dict(zip([d[0] for d in cursor.description], args))
    row = cursor.fetchone()
    datos="delete from empleadosf WHERE codempleado='{}'".format(codempleado)
    print(datos)
    cursor.execute(datos)
    conexion.commit()
    cursor.close()
    conexion.close()
    return row

################################################################################

# Definimos el nombre de la aplicación para flask
app = Flask(__name__)
RESPONSE_BODY_DEFAULT = {"message": "", "data": [], "errors": []}

# Muestra la página principal con su correspondiente plantilla
@app.route('/',methods=["GET","POST"])
def inicio():
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.OK
    connection = pool.acquire()
    cursor = connection.cursor()
    cursor.execute("select * from empleadosf")
    cursor.rowfactory = lambda *args: dict(zip([d[0] for d in cursor.description], args))
    res = cursor.fetchall()
    response_body["data"] = res
    response_body["message"] = "Empleados consultados correctamente"
    return  response_body, status_code

@app.route('/crear',methods=["GET","POST"])
def crear():
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.OK
    if request.method == "POST":
        campos = ['name','last_name','password','email','id']
        for campo in campos:
            if not campo in request.json: 
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code

            campo_actual = request.json[campo]

            if campo_actual == "":
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code
            
        connection = pool.acquire()
        res = insertar(connection, request.json['name'], request.json['last_name'], request.json['password'], request.json['email'], request.json['id'])
        response_body["data"] = res
        response_body["message"] = "Empleado creado correctamente"
        msg = Message('!Registro exitoso!', sender = 'mencinalesr@correo.udistrital.edu.co', recipients = [request.json['email']])
        msg.body = "Gracias por tu registro "+request.json['name']+' '+request.json['last_name']
        mail.send(msg)
        return response_body, status_code

@app.route('/actualizar',methods=["GET","POST"])
def modificar():
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.OK
    if request.method == "POST":
        campos = ['CODEMPLEADO','name','last_name','password','email','id']
        for campo in campos:
            if not campo in request.json: 
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code

            campo_actual = request.json[campo]

            if campo_actual == "":
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code
            else:
                connection = pool.acquire()
                res = actualizar(connection, request.json['CODEMPLEADO'], request.json['name'], request.json['last_name'], request.json['password'], request.json['password'], request.json['id'])
                response_body["data"] = res
                response_body["message"] = "Empleado actualizado correctamente"
            return response_body, status_code

@app.route('/eliminar',methods=["GET","POST"])
def borrar():
    response_body = copy.deepcopy(RESPONSE_BODY_DEFAULT)
    status_code = HTTPStatus.OK
    if request.method == "POST":
        campos = ['CODEMPLEADO']
        for campo in campos:
            if not campo in request.json: 
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code

            campo_actual = request.json[campo]

            if campo_actual == "":
                response_body["errors"].append("Campo "+campo+" es requerido")
                status_code = HTTPStatus.BAD_REQUEST
                return response_body, status_code
            else:
                connection = pool.acquire()
                res = eliminar(connection, request.json['CODEMPLEADO'])
                response_body["data"] = res
                response_body["message"] = "Empleado eliminado correctamente"
            return response_body, status_code
    


## Ver los registros de las tablas

#@app.route('/tabla/<str:nombre>', methods=["GET","POST"])
#def detallejuego(nombre):
#	registros=[]
#
#    connection = pool.acquire()
#    cursor = connection.cursor()
#    cursor.execute("select * from cat")
#    res = cursor.fetchall()
#	
#	for tabla in res:
#        if tabla == str()
#	return render_template("registros.html",registros=registros)

################################################################################


### -Programa principal:

# Este código funciona si se ha ejecutado previamente como usuario startup en la base de datos.
#
if __name__ == '__main__':

    # Iniciar la conexión a la base de datos 
    pool = start_pool()
    # Iniciar la aplicación web
    CORS(app)
    app.config['MAIL_SERVER']='smtp.gmail.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USERNAME'] = 'correo@correo.com'
    app.config['MAIL_PASSWORD'] = '**********'
    app.config['MAIL_USE_TLS'] = False
    app.config['MAIL_USE_SSL'] = True

    mail = Mail(app)
    app.run(port=int(os.environ.get('PORT', '8080')),debug=True)